<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200928020219 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE age_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, min_age INT NOT NULL, max_age INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE competitor CHANGE stripe_customer_id stripe_customer_id VARCHAR(190) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E0D53BAA708DC647 ON competitor (stripe_customer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE age_group');
        $this->addSql('DROP INDEX UNIQ_E0D53BAA708DC647 ON competitor');
        $this->addSql('ALTER TABLE competitor CHANGE stripe_customer_id stripe_customer_id VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
