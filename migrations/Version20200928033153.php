<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200928033153 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE program_day (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE program_group (id INT AUTO_INCREMENT NOT NULL, day_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_F3A937339C24126 (day_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE program_item (id INT AUTO_INCREMENT NOT NULL, program_group_id INT DEFAULT NULL, item VARCHAR(255) NOT NULL, INDEX IDX_12FD70197F612572 (program_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE program_group ADD CONSTRAINT FK_F3A937339C24126 FOREIGN KEY (day_id) REFERENCES program_day (id)');
        $this->addSql('ALTER TABLE program_item ADD CONSTRAINT FK_12FD70197F612572 FOREIGN KEY (program_group_id) REFERENCES program_group (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE program_group DROP FOREIGN KEY FK_F3A937339C24126');
        $this->addSql('ALTER TABLE program_item DROP FOREIGN KEY FK_12FD70197F612572');
        $this->addSql('DROP TABLE program_day');
        $this->addSql('DROP TABLE program_group');
        $this->addSql('DROP TABLE program_item');
    }
}
