<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201008225516 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE class_group_dance_group (class_group_id INT NOT NULL, dance_group_id INT NOT NULL, INDEX IDX_9D811CAD4A9A1217 (class_group_id), INDEX IDX_9D811CAD9EA972BC (dance_group_id), PRIMARY KEY(class_group_id, dance_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE class_group_dance_group ADD CONSTRAINT FK_9D811CAD4A9A1217 FOREIGN KEY (class_group_id) REFERENCES class_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE class_group_dance_group ADD CONSTRAINT FK_9D811CAD9EA972BC FOREIGN KEY (dance_group_id) REFERENCES dance_group (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE class_group_dance_group');
    }
}
