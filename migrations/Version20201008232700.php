<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201008232700 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE competitor_dance_group (competitor_id INT NOT NULL, dance_group_id INT NOT NULL, INDEX IDX_B4E74BF778A5D405 (competitor_id), INDEX IDX_B4E74BF79EA972BC (dance_group_id), PRIMARY KEY(competitor_id, dance_group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE competitor_dance_group ADD CONSTRAINT FK_B4E74BF778A5D405 FOREIGN KEY (competitor_id) REFERENCES competitor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE competitor_dance_group ADD CONSTRAINT FK_B4E74BF79EA972BC FOREIGN KEY (dance_group_id) REFERENCES dance_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE competitor ADD class_id INT DEFAULT NULL, ADD age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE competitor ADD CONSTRAINT FK_E0D53BAAEA000B10 FOREIGN KEY (class_id) REFERENCES class_group (id)');
        $this->addSql('ALTER TABLE competitor ADD CONSTRAINT FK_E0D53BAACC80CD12 FOREIGN KEY (age_id) REFERENCES age_group (id)');
        $this->addSql('CREATE INDEX IDX_E0D53BAAEA000B10 ON competitor (class_id)');
        $this->addSql('CREATE INDEX IDX_E0D53BAACC80CD12 ON competitor (age_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE competitor_dance_group');
        $this->addSql('ALTER TABLE competitor DROP FOREIGN KEY FK_E0D53BAAEA000B10');
        $this->addSql('ALTER TABLE competitor DROP FOREIGN KEY FK_E0D53BAACC80CD12');
        $this->addSql('DROP INDEX IDX_E0D53BAAEA000B10 ON competitor');
        $this->addSql('DROP INDEX IDX_E0D53BAACC80CD12 ON competitor');
        $this->addSql('ALTER TABLE competitor DROP class_id, DROP age_id');
    }
}
