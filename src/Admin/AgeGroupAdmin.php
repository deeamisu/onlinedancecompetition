<?php

namespace App\Admin;
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 10/6/2020
 * Time: 3:32 AM
 */

use App\Entity\AgeGroup;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Templating\TemplateRegistry;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class AgeGroupAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Content',[
                'class' => 'col-md-6'
            ])
                ->add('name', TextType::class)
                ->add('minAge',IntegerType::class)
                ->add('maxAge',IntegerType::class)
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('minAge')
            ->add('maxAge')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', TemplateRegistry::TYPE_STRING, [
                'header_style' => 'width: 20%; text-align: center',
                'row_align' => 'center'
            ])
            ->add('minAge', TemplateRegistry::TYPE_INTEGER, [
                'header_style' => 'width: 15%; text-align: center',
                'row_align' => 'center'
            ])
            ->add('maxAge', TemplateRegistry::TYPE_INTEGER, [
                'header_style' => 'width: 15%; text-align: center',
                'row_align' => 'center'
            ])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            //->add('id')
            ->add('name')
            ->add('minAge')
            ->add('maxAge')
        ;
    }

    public function toString($object)
    {
        return $object instanceof AgeGroup
            ? $object->getName()
            : 'Age Group'; // shown in the breadcrumb on the create view
    }
}