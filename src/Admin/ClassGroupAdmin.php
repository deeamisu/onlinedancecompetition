<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\ClassGroup;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;

final class ClassGroupAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            //->add('id')
            ->add('name')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            //->add('id')
            ->addIdentifier('name')
            ->add('dance')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('Content', [
                'class' => 'col-md-6'
            ])
                //->add('id')
                ->add('name')
            ->end()
            ->with('Metadata', [
                'class' => 'col-md-6'
            ])
                ->add('dance', ModelType::class, [
                    'expanded' => true,
                    'multiple' => true
                ])
            ->end()
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            //->add('id')
            ->add('name')
            ->add('dance')
            ;
    }

    public function toString($object)
    {
        return $object instanceof ClassGroup
            ? $object->getName()
            : 'Class Group'; // shown in the breadcrumb on the create view
    }
}
