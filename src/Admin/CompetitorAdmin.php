<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\AgeGroup;
use App\Entity\ClassGroup;
use App\Entity\Competitor;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;

final class CompetitorAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            //->add('id')
            ->add('gentlemanName')
            ->add('gentlemanSurname')
            ->add('ladyName')
            ->add('ladySurname')
            ->add('email')
            //->add('stripeCustomerId')
            ->add('phone')
            ->add('payment')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            //->add('id')
            ->add('gentlemanName')
            ->add('gentlemanSurname')
            ->add('ladyName')
            ->add('ladySurname')
            ->add('email')
            //->add('stripeCustomerId')
            ->add('phone')
            ->add('payment')
            ->add('class')
            ->add('age')
            ->add('dance')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('Content', [
                'class' => 'col-md-6'
            ])
                //->add('id')
                ->add('gentlemanName')
                ->add('gentlemanSurname')
                ->add('ladyName')
                ->add('ladySurname')
                ->add('email')
                //->add('stripeCustomerId')
                ->add('phone')
                ->add('payment')
            ->end()
            ->with('Metadata', [
                'class' => 'col-md-6'
            ])
                ->add('class', ModelType::class, [
                    'class' => ClassGroup::class,
                    'property' => 'name'
                ])
                ->add('age', ModelType::class, [
                    'class' => AgeGroup::class,
                    'property' => 'name'
                ])
                ->add('dance', ModelType::class, [
                    'expanded' => true,
                    'multiple' => true
                ])
            ->end()
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->with('Competitor',[
                'class' => 'col-md-6'
            ])
                //->add('id')
                ->add('gentlemanName')
                ->add('gentlemanSurname')
                ->add('ladyName')
                ->add('ladySurname')
                ->add('email')
                //->add('stripeCustomerId')
                ->add('phone')
                ->add('payment')
                ->add('class')
                ->add('age')
                ->add('dance')
            ->end()
            ;
    }

    public function toString($object)
    {
        return $object instanceof Competitor
            ? $object->view()
            : 'Competitor'; // shown in the breadcrumb on the create view
    }
}
