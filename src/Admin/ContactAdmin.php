<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Contact;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class ContactAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            //->add('id')
            ->add('role')
            ->add('name')
            ->add('phone')
            ->add('mail')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            //->add('id')
            ->add('role')
            ->add('name')
            ->add('phone')
            ->add('mail')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            //->add('id')
            ->add('role')
            ->add('name')
            ->add('phone')
            ->add('mail')
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            //->add('id')
            ->add('role')
            ->add('name')
            ->add('phone')
            ->add('mail')
            ;
    }

    public function toString($object)
    {
        return $object instanceof Contact
            ? $object->getName()
            : 'Contact'; // shown in the breadcrumb on the create view
    }
}
