<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Result;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class ResultAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('filename')
            ->add('updated')
            ->add('filepath')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('filename')
            ->add('updated')
            ->add('filepath')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            //->add('id')
            //->add('filename')
            //->add('updated')
            //->add('filepath')
            ->add('file', FileType::class, [
                'required' => false
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('filename')
            ->add('updated')
            ->add('filepath')
            ;
    }

    public function toString($object)
    {
        return $object instanceof Result
            ? $object->getFilename()
            : 'Result file'; // shown in the breadcrumb on the create view
    }

    public function prePersist($image)
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image)
    {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image)
    {
        /**
         * @var $image Result
         */
        if ($image->getFile()) {
            $image->setRootDir($this->rootDir);
            $image->refreshUpdated();
        }
    }
}
