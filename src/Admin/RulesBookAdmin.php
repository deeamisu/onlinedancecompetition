<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Language;
use App\Entity\RulesBook;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

final class RulesBookAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            //->add('id')
            ->add('filename')
            ->add('updated')
            ->add('filepath')
            ->add('language',null, [], EntityType::class, [
                'class' => Language::class,
                'choice_label' => 'name'
            ])
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            //->add('id')
            ->add('filename')
            ->add('updated')
            ->add('filepath')
            ->add('language')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->with('File', [
                'class' => 'col-md-9',
            ])
            //->add('id')
            //->add('filename')
            //->add('updated')
            //->add('filepath')
            ->add('file', FileType::class, [
                'required' => false
            ])
            ->end()
            ->with('Metadata', [
                'class' => 'col-md-3'
            ])
            ->add('language', ModelType::class, [
                'class' => Language::class,
                'property' => 'name'
            ])
            ->end()
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->with('Content', [
                'class' => 'col-md-9'
            ])
            //->add('id')
            ->add('filename')
            ->add('updated')
            ->add('filepath')
            ->end()
            ->with('Metadata', [
                'class' => 'col-md-3'
            ])
            ->add('language')
            ->end()
            ;
    }

    public function toString($object)
    {
        return $object instanceof RulesBook
            ? $object->getFilename()
            : 'Rules Book file'; // shown in the breadcrumb on the create view
    }

    public function prePersist($image)
    {
        $this->manageFileUpload($image);
    }

    public function preUpdate($image)
    {
        $this->manageFileUpload($image);
    }

    private function manageFileUpload($image)
    {
        /**
         * @var $image RulesBook
         */
        if ($image->getFile()) {
            $image->setRootDir($this->rootDir);
            $image->refreshUpdated();
        }
    }
}
