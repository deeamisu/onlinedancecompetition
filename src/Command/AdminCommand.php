<?php

namespace App\Command;

use App\Service\GeneralService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AdminCommand extends Command
{
    protected static $defaultName = 'app:admin';

    private $generalService;

    /**
     * AdminCommand constructor.
     * @param $generalService GeneralService
     */
    public function __construct(GeneralService $generalService)
    {
        parent::__construct();
        $this->generalService = $generalService;
    }


    protected function configure()
    {
        $this
            ->setDescription('Creates and deletes admin')
            ->addArgument('username', InputArgument::REQUIRED, 'Admin username')
            ->addArgument('password', InputArgument::OPTIONAL, 'Admin password')
            ->addOption('create', 'c', InputOption::VALUE_NONE, 'Creates a new admin')
            ->addOption('delete', 'd', InputOption::VALUE_NONE, 'Deletes an admin')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('username');
        $arg2 = $input->getArgument('password');

        if ($input->getOption('create')) {
            $io->title('Creating new Admin '.$arg1);
            if (!$arg2){
                $arg2 = '1234';
            }
            $this->generalService->newAdmin($arg1,$arg2);
            $io->success('Admin created');
            return 0;
        }
        elseif ($input->getOption('delete'))  {
            $io->title('Deleting Admin '.$arg1);
            $this->generalService->deleteAdmin($arg1);
            $io->success('Admin deleted');
            return 0;
        }
        return 0;
    }
}
