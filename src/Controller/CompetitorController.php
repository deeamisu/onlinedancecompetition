<?php

namespace App\Controller;

use App\Entity\Competitor;
use App\Form\CompetitorType;
use App\Repository\CompetitorRepository;
use App\Service\GeneralService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/competitor")
 */
class CompetitorController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/", name="competitor_index", methods={"GET"})
     */
    public function index(CompetitorRepository $competitorRepository): Response
    {
        return $this->render('competitor/index.html.twig', [
            'competitors' => $competitorRepository->findAll(),
        ]);
    }


    /**
     * @Route("/new", name="competitor_new", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $competitor = new Competitor();
        $form = $this->createForm(CompetitorType::class, $competitor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $competitor->setPayment('Not Payed');
            $entityManager->persist($competitor);
            $entityManager->flush();
            $dances = $competitor->getClass()->getDance();

            return $this->render('competitor/show.html.twig', [
                'competitor' => $competitor,
                'dances' => $dances
            ]);
        }

        return $this->render('competitor/new.html.twig', [
            'competitor' => $competitor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="competitor_show", methods={"GET"})
     */
    public function show(Competitor $competitor): Response
    {
        return $this->render('competitor/show.html.twig', [
            'competitor' => $competitor,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="competitor_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Competitor $competitor): Response
    {
        $form = $this->createForm(CompetitorType::class, $competitor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('competitor_index');
        }

        return $this->render('competitor/edit.html.twig', [
            'competitor' => $competitor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="competitor_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Competitor $competitor): Response
    {
        if ($this->isCsrfTokenValid('delete'.$competitor->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($competitor);
            $entityManager->flush();
        }

        return $this->redirectToRoute('competitor_index');
    }

    /**
     * @Route("/competitor/dances/{id}", name="competitor_dances", methods={"POST"})
     */
    public function setDances($id, GeneralService $generalService)
    {
        $dances = $_POST['dances'];
        $generalService->setCompetitorDances($id, $dances);
        return $this->redirectToRoute('order_checkout',[
            'id' => $id
        ]);
    }
}
