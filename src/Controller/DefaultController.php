<?php

namespace App\Controller;

use App\Entity\Competitor;
use App\Entity\Contact;
use App\Entity\ProgramDay;
use App\Entity\Result;
use App\Service\GeneralService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/program/{day}", name="program")
     */
    public function Program($day)
    {
        $program = $this->getDoctrine()->getRepository(ProgramDay::class)->findOneBy(['name' => $day]);
        return $this->render('default/program.html.twig',[
           'program' => $program
        ]);
    }

    /**
     * @Route("/regulament", name="regulament")
     */
    public function regulament(GeneralService $generalService)
    {
        return $this->render('default/regulament.html.twig',[
           'regulament' => $generalService->getRegulament()
        ]);
    }

    /**
     * @Route("/invitation", name="invitation")
     */
    public function invitation(GeneralService $generalService)
    {
        return $this->render('default/invitation.html.twig',[
            'invitation' => $generalService->getInvitation()
        ]);
    }

    /**
     * @Route("/succes/payment/{id}", name="succes")
     */
    public function succes($id)
    {
        /**
         * @var $competitor Competitor
         */
        $competitor = $this->getDoctrine()->getRepository(Competitor::class)->find($id);
        $competitor->setPayment('Achitat');
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('registered_competitor');
    }

    /**
     * @Route("/cancel", name="cancel")
     */
    public function cancel()
    {
        return $this->render('stripe/cancel.html.twig');
    }

    /**
     * @Route("/registered/competitor", name="registered_competitor")
     */
    public function registeredCompetitors()
    {
        $competitors = $this->getDoctrine()->getRepository(Competitor::class)->findBy([
            'payment' => 'Achitat'],['id' => 'DESC']);
        return $this->render('default/registered_competitors.html.twig',[
           'competitors' => $competitors
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        $contacts = $this->getDoctrine()->getRepository(Contact::class)->findAll();
        return $this->render('default/contact.html.twig', [
            'contacts' => $contacts
        ]);
    }

    public function domain()
    {
        return $this->redirect('index.php/regulament');
    }

    /**
     * @Route("/flag/{language}", name="flag_language")
     */
    public function flagLanguage($language)
    {
        $session = $this->get('session');
        $session->set('language',$language);
        return $this->redirectToRoute('regulament');
    }

    /**
     * @Route("/results", name="results")
     */
    public function results()
    {
        return $this->render('default/results.html.twig', [
            'results' => $this->getDoctrine()->getRepository(Result::class)->findAll()
        ]);
    }
}
