<?php

namespace App\Controller;

use App\Entity\Competitor;
use App\Service\GeneralService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/registration", name="registration")
     */
    public function index()
    {
        return $this->render('registration/index.html.twig', [
            'controller_name' => 'RegistrationController',
        ]);
    }

    /**
     * @Route("/checkout/{id}", name="order_checkout")
     */
    public function checkoutAction($id, Request $request, GeneralService $generalService)
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var $competitor Competitor
         */
        $competitor = $em->find(Competitor::class,$id);
        $competitionTax = $generalService->competitionFee($competitor);


        \Stripe\Stripe::setApiKey($this->getParameter('stripe_secret_key'));

        /**
        if ($request->isMethod('POST')) {
            $token = $request->request->get('stripeToken');
            \Stripe\Stripe::setApiKey($this->getParameter('stripe_secret_key'));

            if (!$competitor->getStripeCustomerId()) {
                $customer = \Stripe\Customer::create([
                    'email' => $competitor->getEmail(),
                    'source' => $token
                ]);
                $competitor->setStripeCustomerId($customer->id);
                $competitor->setPayment('Achitat');
                $em->flush();
            } else {

                /**
                 * $customer = \Stripe\Customer::retrieve($competitor->getStripeCustomerId());
                 * $customer->source = $token;
                 * $customer->save();
                 * }
                 *
                 * \Stripe\Charge::create(array(
                 * "amount" => $competitionTax,
                 * "currency" => "ron",
                 * "customer" => $competitor->getStripeCustomerId(),
                 * "description" => "Taxa Inscriere"
                 * ));
                 */

                $session = \Stripe\Checkout\Session::create([
                    'payment_method_types' => ['card'],
                    'line_items' => [[
                        'price_data' => [
                            'product' => 'prod_IBwyt69I73r49O',
                            'unit_amount' => $competitionTax,
                            'currency' => 'ron',
                        ],
                        'quantity' => 1,
                    ]],
                    'mode' => 'payment',
                    'success_url' => 'https://www.grandprixonline.eu/index.php/succes/payment/'.$competitor->getId(),
                    'cancel_url' => 'https://www.grandprixonline.eu/index.php/cancel',
                ]);
            //}
            /**
            $this->addFlash('success', 'Order Complete! Yay!');
            return $this->redirectToRoute('regulament');
        }*/

        return $this->render('stripe/checkout.html.twig', [
            'stripe_public_key' => $this->getParameter('stripe_public_key'),
            'tax' => $competitionTax,
            'CHECKOUT_SESSION_ID' => $session->id
        ]);
    }
}
