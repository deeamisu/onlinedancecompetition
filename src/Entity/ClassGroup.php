<?php

namespace App\Entity;

use App\Repository\ClassGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClassGroupRepository::class)
 */
class ClassGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=DanceGroup::class, inversedBy="class")
     */
    private $dance;

    /**
     * @ORM\OneToMany(targetEntity=Competitor::class, mappedBy="class")
     */
    private $competitors;

    public function __construct()
    {
        $this->dance = new ArrayCollection();
        $this->competitors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|DanceGroup[]
     */
    public function getDance(): Collection
    {
        return $this->dance;
    }

    public function addDance(DanceGroup $dance): self
    {
        if (!$this->dance->contains($dance)) {
            $this->dance[] = $dance;
        }

        return $this;
    }

    public function removeDance(DanceGroup $dance): self
    {
        if ($this->dance->contains($dance)) {
            $this->dance->removeElement($dance);
        }

        return $this;
    }

    /**
     * @return Collection|Competitor[]
     */
    public function getCompetitors(): Collection
    {
        return $this->competitors;
    }

    public function addCompetitor(Competitor $competitor): self
    {
        if (!$this->competitors->contains($competitor)) {
            $this->competitors[] = $competitor;
            $competitor->setClass($this);
        }

        return $this;
    }

    public function removeCompetitor(Competitor $competitor): self
    {
        if ($this->competitors->contains($competitor)) {
            $this->competitors->removeElement($competitor);
            // set the owning side to null (unless already changed)
            if ($competitor->getClass() === $this) {
                $competitor->setClass(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        return $this->getName();
    }


}
