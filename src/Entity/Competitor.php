<?php

namespace App\Entity;

use App\Repository\CompetitorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompetitorRepository::class)
 */
class Competitor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gentlemanName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gentlemanSurname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ladyName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ladySurname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true, length=190)
     */
    private $stripeCustomerId;

    /**
     * @ORM\ManyToOne(targetEntity=ClassGroup::class, inversedBy="competitors")
     */
    private $class;

    /**
     * @ORM\ManyToOne(targetEntity=AgeGroup::class, inversedBy="competitors")
     */
    private $age;

    /**
     * @ORM\ManyToMany(targetEntity=DanceGroup::class, inversedBy="competitors")
     */
    private $dance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payment;

    public function __construct()
    {
        $this->dance = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGentlemanName(): ?string
    {
        return $this->gentlemanName;
    }

    public function setGentlemanName(?string $gentlemanName): self
    {
        $this->gentlemanName = $gentlemanName;

        return $this;
    }

    public function getGentlemanSurname(): ?string
    {
        return $this->gentlemanSurname;
    }

    public function setGentlemanSurname(?string $gentlemanSurname): self
    {
        $this->gentlemanSurname = $gentlemanSurname;

        return $this;
    }

    public function getLadyName(): ?string
    {
        return $this->ladyName;
    }

    public function setLadyName(?string $ladyName): self
    {
        $this->ladyName = $ladyName;

        return $this;
    }

    public function getLadySurname(): ?string
    {
        return $this->ladySurname;
    }

    public function setLadySurname(?string $ladySurname): self
    {
        $this->ladySurname = $ladySurname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getStripeCustomerId()
    {
        return $this->stripeCustomerId;
    }
    public function setStripeCustomerId($stripeCustomerId)
    {
        $this->stripeCustomerId = $stripeCustomerId;
    }

    public function getClass(): ?ClassGroup
    {
        return $this->class;
    }

    public function setClass(?ClassGroup $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getAge(): ?AgeGroup
    {
        return $this->age;
    }

    public function setAge(?AgeGroup $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return Collection|DanceGroup[]
     */
    public function getDance(): Collection
    {
        return $this->dance;
    }

    public function addDance(DanceGroup $dance): self
    {
        if (!$this->dance->contains($dance)) {
            $this->dance[] = $dance;
        }

        return $this;
    }

    public function removeDance(DanceGroup $dance): self
    {
        if ($this->dance->contains($dance)) {
            $this->dance->removeElement($dance);
        }

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function view ()
    {
        return $this->getGentlemanName().' '.$this->getGentlemanSurname().' - '.$this->getLadyName().' '.$this->getLadySurname();
    }

    public function getPayment(): ?string
    {
        return $this->payment;
    }

    public function setPayment(?string $payment): self
    {
        $this->payment = $payment;

        return $this;
    }
}
