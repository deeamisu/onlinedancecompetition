<?php

namespace App\Entity;

use App\Repository\DanceGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DanceGroupRepository::class)
 */
class DanceGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=ClassGroup::class, mappedBy="dance")
     */
    private $class;

    /**
     * @ORM\ManyToMany(targetEntity=Competitor::class, mappedBy="dance")
     */
    private $competitors;

    public function __construct()
    {
        $this->class = new ArrayCollection();
        $this->competitors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ClassGroup[]
     */
    public function getClass(): Collection
    {
        return $this->class;
    }

    public function addClass(ClassGroup $class): self
    {
        if (!$this->class->contains($class)) {
            $this->class[] = $class;
            $class->addDance($this);
        }

        return $this;
    }

    public function removeClass(ClassGroup $class): self
    {
        if ($this->class->contains($class)) {
            $this->class->removeElement($class);
            $class->removeDance($this);
        }

        return $this;
    }

    function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection|Competitor[]
     */
    public function getCompetitors(): Collection
    {
        return $this->competitors;
    }

    public function addCompetitor(Competitor $competitor): self
    {
        if (!$this->competitors->contains($competitor)) {
            $this->competitors[] = $competitor;
            $competitor->addDance($this);
        }

        return $this;
    }

    public function removeCompetitor(Competitor $competitor): self
    {
        if ($this->competitors->contains($competitor)) {
            $this->competitors->removeElement($competitor);
            $competitor->removeDance($this);
        }

        return $this;
    }


}
