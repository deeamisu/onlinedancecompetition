<?php

namespace App\Entity;

use App\Repository\LanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LanguageRepository::class)
 */
class Language
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=RulesBook::class, mappedBy="language")
     */
    private $rulesBooks;

    /**
     * @ORM\OneToMany(targetEntity=Invitation::class, mappedBy="language")
     */
    private $invitations;

    public function __construct()
    {
        $this->rulesBooks = new ArrayCollection();
        $this->invitations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|RulesBook[]
     */
    public function getRulesBooks(): Collection
    {
        return $this->rulesBooks;
    }

    public function addRulesBook(RulesBook $rulesBook): self
    {
        if (!$this->rulesBooks->contains($rulesBook)) {
            $this->rulesBooks[] = $rulesBook;
            $rulesBook->setLanguage($this);
        }

        return $this;
    }

    public function removeRulesBook(RulesBook $rulesBook): self
    {
        if ($this->rulesBooks->contains($rulesBook)) {
            $this->rulesBooks->removeElement($rulesBook);
            // set the owning side to null (unless already changed)
            if ($rulesBook->getLanguage() === $this) {
                $rulesBook->setLanguage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Invitation[]
     */
    public function getInvitations(): Collection
    {
        return $this->invitations;
    }

    public function addInvitation(Invitation $invitation): self
    {
        if (!$this->invitations->contains($invitation)) {
            $this->invitations[] = $invitation;
            $invitation->setLanguage($this);
        }

        return $this;
    }

    public function removeInvitation(Invitation $invitation): self
    {
        if ($this->invitations->contains($invitation)) {
            $this->invitations->removeElement($invitation);
            // set the owning side to null (unless already changed)
            if ($invitation->getLanguage() === $this) {
                $invitation->setLanguage(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        return $this->getName();
    }


}
