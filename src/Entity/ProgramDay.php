<?php

namespace App\Entity;

use App\Repository\ProgramDayRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProgramDayRepository::class)
 */
class ProgramDay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=ProgramGroup::class, mappedBy="day")
     */
    private $programGroups;

    public function __construct()
    {
        $this->programGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|ProgramGroup[]
     */
    public function getProgramGroups(): Collection
    {
        return $this->programGroups;
    }

    public function addProgramGroup(ProgramGroup $programGroup): self
    {
        if (!$this->programGroups->contains($programGroup)) {
            $this->programGroups[] = $programGroup;
            $programGroup->setDay($this);
        }

        return $this;
    }

    public function removeProgramGroup(ProgramGroup $programGroup): self
    {
        if ($this->programGroups->contains($programGroup)) {
            $this->programGroups->removeElement($programGroup);
            // set the owning side to null (unless already changed)
            if ($programGroup->getDay() === $this) {
                $programGroup->setDay(null);
            }
        }

        return $this;
    }
}
