<?php

namespace App\Entity;

use App\Repository\ProgramGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProgramGroupRepository::class)
 */
class ProgramGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=ProgramDay::class, inversedBy="programGroups")
     */
    private $day;

    /**
     * @ORM\OneToMany(targetEntity=ProgramItem::class, mappedBy="programGroup")
     */
    private $programItems;

    public function __construct()
    {
        $this->programItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDay(): ?ProgramDay
    {
        return $this->day;
    }

    public function setDay(?ProgramDay $day): self
    {
        $this->day = $day;

        return $this;
    }

    /**
     * @return Collection|ProgramItem[]
     */
    public function getProgramItems(): Collection
    {
        return $this->programItems;
    }

    public function addProgramItem(ProgramItem $programItem): self
    {
        if (!$this->programItems->contains($programItem)) {
            $this->programItems[] = $programItem;
            $programItem->setProgramGroup($this);
        }

        return $this;
    }

    public function removeProgramItem(ProgramItem $programItem): self
    {
        if ($this->programItems->contains($programItem)) {
            $this->programItems->removeElement($programItem);
            // set the owning side to null (unless already changed)
            if ($programItem->getProgramGroup() === $this) {
                $programItem->setProgramGroup(null);
            }
        }

        return $this;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getName();
    }


}
