<?php

namespace App\Entity;

use App\Repository\ProgramItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProgramItemRepository::class)
 */
class ProgramItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity=ProgramGroup::class, inversedBy="programItems")
     */
    private $programGroup;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getItem(): ?string
    {
        return $this->item;
    }

    public function setItem(string $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getProgramGroup(): ?ProgramGroup
    {
        return $this->programGroup;
    }

    public function setProgramGroup(?ProgramGroup $programGroup): self
    {
        $this->programGroup = $programGroup;

        return $this;
    }

    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getItem();
    }


}
