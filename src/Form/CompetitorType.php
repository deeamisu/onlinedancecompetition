<?php

namespace App\Form;

use App\Entity\Competitor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompetitorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gentlemanName')
            ->add('gentlemanSurname')
            ->add('ladyName')
            ->add('ladySurname')
            ->add('email')
            ->add('phone')
            ->add('class')
            ->add('age')
            //->add('stripeCustomerId')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Competitor::class,
        ]);
    }
}
