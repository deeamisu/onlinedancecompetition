<?php

namespace App\Repository;

use App\Entity\DanceGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DanceGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method DanceGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method DanceGroup[]    findAll()
 * @method DanceGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DanceGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DanceGroup::class);
    }

    // /**
    //  * @return DanceGroup[] Returns an array of DanceGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DanceGroup
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
