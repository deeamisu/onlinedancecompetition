<?php

namespace App\Repository;

use App\Entity\ProgramDay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProgramDay|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProgramDay|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProgramDay[]    findAll()
 * @method ProgramDay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProgramDayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProgramDay::class);
    }

    // /**
    //  * @return ProgramDay[] Returns an array of ProgramDay objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProgramDay
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
