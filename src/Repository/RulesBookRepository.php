<?php

namespace App\Repository;

use App\Entity\RulesBook;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RulesBook|null find($id, $lockMode = null, $lockVersion = null)
 * @method RulesBook|null findOneBy(array $criteria, array $orderBy = null)
 * @method RulesBook[]    findAll()
 * @method RulesBook[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RulesBookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RulesBook::class);
    }

    // /**
    //  * @return RulesBook[] Returns an array of RulesBook objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RulesBook
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
