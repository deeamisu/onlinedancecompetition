<?php

namespace App\Service;

/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 9/8/2020
 * Time: 9:54 PM
 */

use App\Entity\Admin;
use App\Entity\Competitor;
use App\Entity\DanceGroup;
use App\Entity\Invitation;
use App\Entity\Language;
use App\Entity\RulesBook;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class GeneralService
{
    private $passwordEncoder;

    private $entityManager;

    private $parameterBag;

    private $session;

    /**
     * GeneralService constructor.
     * @param $passwordEncoder UserPasswordEncoderInterface
     * @param $entityManager EntityManagerInterface
     * @param $parameterBag ParameterBagInterface
     * @param $session SessionInterface
     */
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager,
        ParameterBagInterface $parameterBag,
        SessionInterface $session
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->session = $session;
    }

    public function newAdmin($username,$password)
    {
        $admin = new Admin();
        $roles = [];
        $roles[]= 'ROLE_ADMIN';
        $admin->setRoles($roles);
        $admin->setUsername($username);
        $admin->setPassword($this->passwordEncoder->encodePassword($admin,$password));
        $this->entityManager->persist($admin);
        $this->entityManager->flush();
    }

    public function deleteAdmin($username)
    {
        $admin = $this->entityManager->getRepository(Admin::class)->findAdminByUsername($username);
        $this->entityManager->remove($admin);
        $this->entityManager->flush();
    }

    private function getRootDir()
    {
        return $this->parameterBag->get('kernel.project_dir');
    }

    public function getRegulament()
    {
        if ($this->session->get('language') == 'EN') {
            $language = $this->entityManager->getRepository(Language::class)->findOneBy(['name' => 'EN']);
        }
        else {
            $language = $this->entityManager->getRepository(Language::class)->findOneBy(['name' => 'RO']);
        }
        /**
         * @var $ruleBooks RulesBook[]
         * @var $language Language
         */
        $ruleBooks = $language->getRulesBooks();
        if (count($ruleBooks) == 1 ) {
            $filepath = $ruleBooks[0]->getFilepath();
            $converter = new DocxToTextConversion($filepath);
            return $converter->convertToText();
        }
    }

    public function getInvitation()
    {
        if ($this->session->get('language') == 'EN') {
            $language = $this->entityManager->getRepository(Language::class)->findOneBy(['name' => 'EN']);
        }
        else {
            $language = $this->entityManager->getRepository(Language::class)->findOneBy(['name' => 'RO']);
        }
        /**
         * @var $invitations Invitation[]
         * @var $language Language
         */
        $invitations = $language->getInvitations();
        if (count($invitations) == 1 ) {
            return 'invitation/'.$invitations[0]->getFilename();
        }
    }

    public function setCompetitorDances($id, $dances)
    {
        /**
         * @var $competitor Competitor
         */
        $competitor = $this->entityManager->find(Competitor::class, $id);
        foreach ($dances as $dance) {
            $danceGroup = $this->entityManager->getRepository(DanceGroup::class)->findOneBy(['name' => $dance]);
            $competitor->addDance($danceGroup);
            $this->entityManager->flush();
        }
    }

    /**
     * @param $competitor Competitor
     */
    public function competitionFee($competitor)
    {
        $danceNumber = count($competitor->getDance());
        return $danceNumber * 2000;
    }
}